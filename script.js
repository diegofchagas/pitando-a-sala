
//Criando as variáveis necessarias!
const janelaTamanho = 2.0 * 1.2;
const portaTamanho = 0.8 * 1.9;
const altura = document.querySelector("#altura");
const largura = document.querySelector("#largura");
const portas = document.querySelector("#portas");
const janelas = document.querySelector("#janelas");
const parede1 = document.querySelector(".parede1");

const altura2 = document.querySelector("#altura2");
const largura2 = document.querySelector("#largura2");
const portas2 = document.querySelector("#portas2");
const janelas2 = document.querySelector("#janelas2");
const parede2 = document.querySelector(".parede2");

const altura3 = document.querySelector("#altura3");
const largura3 = document.querySelector("#largura3");
const portas3 = document.querySelector("#portas3");
const janelas3 = document.querySelector("#janelas3");
const parede3 = document.querySelector(".parede3");

const altura4 = document.querySelector("#altura4");
const largura4 = document.querySelector("#largura4");
const portas4 = document.querySelector("#portas4");
const janelas4 = document.querySelector("#janelas4");
const parede4 = document.querySelector(".parede4");

const parede = document.querySelector("#resposta-paredes");
const tinta = document.querySelector("#tinta");
const btn = document.querySelector("#btn");

// Condições criada para o codigo trazer a resposta final so se todas as condicções forem atendidas
let cond1 = false;
let cond2 = false;
let cond3 = false;
let cond4 = false;
// Variável que recebe os tamnhps de cada parede e armazena para a soma total da area
let total = [0, 0, 0, 0];

/* botao com efeito de click para fazer o calculo da área e trazer a resposta para o usuário, onde a partir daqui toda
verificação é feita*/
btn.addEventListener("click", () => {
  //parede 1

  // verificando se parede tem a metragem necessária para fazer os cálculos
  if (altura.value * largura.value >= 1 && altura.value * largura.value <= 15) {
     // verificando se as janelas e portas não ultrapassam o valor de 50% do tamango da parede
    if (
      (altura.value * largura.value) / 2 >=
      janelaTamanho * (janelas.value || 0) + portaTamanho * (portas.value || 0)
    ) {
      // verificando os valor de porta e se altura da parede está 0.30 cm mais alta que a porta
      if (portas.value != "") {
        // recendo  os valores passados pelo usuário e abatendo se houver porta ou janelas e passando o valor da area da parede
        if (altura.value >= 2.2) {
         // total[0] = altura.value * largura.value - (janelaTamanho * janelas.value + portaTamanho * portas.value);

          parede1.innerHTML = "";
          cond1 = true;

          // elses para informar algum erro de quantidade de porta ou janelas inapropriadas para o tamanho da parede
        } else {
          funcoes.errorPorta()
        }
        // total da parede , calcula a parede e guarda no array, caso nao tenha porta!
      } else {
        total[0] = altura.value * largura.value - janelaTamanho * janelas.value;
        cond1 = true;
        parede1.innerHTML = "";
      }
    } else {
      funcoes.errorPJ()
    }
  } else {
      funcoes.errorTamanho()
  }

  // parede2
  if (
    altura2.value * largura2.value >= 1 &&
    altura2.value * largura2.value <= 15
  ) {
    if (
      (altura2.value * largura2.value) / 2 >=
      janelaTamanho * (janelas2.value || 0) +
        portaTamanho * (portas2.value || 0)
    ) {
      if (portas2.value != "") {
        if (altura2.value >= 2.2) {
          total[1] =
            altura2.value * largura2.value -
            (janelaTamanho * janelas2.value + portaTamanho * portas2.value);
          parede2.innerHTML = "";
          cond2 = true;
        } else {

          funcoes.errorPorta2()
        }
      } else {
        total[1] =
          altura2.value * largura2.value - janelaTamanho * janelas2.value;
        cond2 = true;
        parede2.innerHTML = "";
      }
    } else {
      funcoes.errorPJ2()
    }
  } else {
    funcoes.errorTamanho2()
  }

  // parede 3
  if (
    altura3.value * largura3.value >= 1 &&
    altura3.value * largura3.value <= 15
  ) {
    if (
      (altura3.value * largura3.value) / 2 >=
      janelaTamanho * (janelas3.value || 0) +
        portaTamanho * (portas3.value || 0)
    ) {
      if (portas3.value != "") {
        if (altura3.value >= 2.2) {
          total[2] =
            altura3.value * largura.value -
            (janelaTamanho * janelas3.value + portaTamanho * portas3.value);

          //console.log(total)
          parede3.innerHTML = "";
          cond3 = true;
        } else {
          funcoes.errorPorta3()
        }
      } else {
        total[2] =
          altura3.value * largura3.value - janelaTamanho * janelas3.value;
        cond3 = true;
        parede3.innerHTML = "";
      }
    } else {
      funcoes.errorPJ3()
    }
  } else {
    funcoes.errorTamanho3()
  }

  // parede 4
  if (
    altura4.value * largura4.value >= 1 &&
    altura4.value * largura4.value <= 15
  ) {
    if (
      (altura4.value * largura4.value) / 2 >=
      janelaTamanho * (janelas4.value || 0) +
        portaTamanho * (portas4.value || 0)
    ) {
      if (portas4.value != "") {
        if (altura4.value >= 2.2) {
          total[3] =
            altura4.value * largura4.value -
            (janelaTamanho * janelas4.value + portaTamanho * portas4.value);

          //console.log(total)
          parede4.innerHTML = "";
          cond4 = true;
        } else {
            funcoes.errorPorta4()
        }
      } else {
        total[3] =
          altura4.value * largura4.value - janelaTamanho * janelas4.value;
        cond4 = true;
        parede4.innerHTML = "";
      }
    } else {
        funcoes.errorPJ4()
    }
  } else {
      funcoes.errorTamanho4()
  }

  // Calculo do valor do total da área para que o usuário veja o tamanho total da sua sala!
  let totalArea = total[0] + total[1] + total[2] + total[3];

// Calculo do valor do total da área / 5 porque sabemos que 1 litro de tinta podemos pintar 5 metros quadrado
  let qtdLitros = (total[0] + total[1] + total[2] + total[3]) / 5;

  console.log(qtdLitros);

  /* variáveis criadas para auxiliar na quantidade de litros de tinta que o usuário vai precisar para pintar sua sala, de acordo o tamanho da sua área total, exemplo 19 litros o usuario irá precisa de 18l + 2latas de 0,5L */
  
  let lata05 = 0;
  let lata2_5 = 0;
  let lata3_6 = 0;
  let lata18 = 0;

/* No caso enquanto a quantidade de litros for > 0 ele entra no if e pega o valor passado e vai retirando ou acrescentando de acordo com o tamanho das latas disponíveis e quantidade de litros que o usuário irá utulizar*/
  while (qtdLitros > 0) {
    if (qtdLitros >= 18) {
      lata18++;
      qtdLitros -= 18;
      continue;
    }
    if (qtdLitros >= 3.6) {
      lata3_6++;
      qtdLitros -= 3.6;
      continue;
    }
    if (qtdLitros >= 2.5) {
      lata2_5++;
      qtdLitros -= 2.5;
      continue;
    }
    if (qtdLitros >= 0.5) {
      lata05++;
      qtdLitros -= 0.5;
      continue;
    }
    if (qtdLitros < 0.5) {
      lata05++;
      qtdLitros = 0;
      continue;
    }
  }

  // Aqui temos todas as informações que é passada para o usuário na tela de acordo com o que o programa definiu e o mesmo ira precisar
  let resposta = "Você vai precisa de ";

  /* Essa vairiável maisLata foi criada com intuito de definição por latas a mais que usuário vai utilizar , como as latas tem tamanho específico depende do tamanho de sua área o mesmo vai precisar de 2 latas ou mais de uma mesma quantidade ou de tamnhos de latas diferentes de acordo com que foi pedido no Code Changelle*/
  /* Exemplo
  44m2 = 2 latas de 3,6L de tinta, + 4 latas de 0,5L de tinta*/

  let maisLata = false;

  if (lata18 > 0) {
    if (lata18 > 1) {
      resposta += `${lata18} latas de 18 litros de tinta,`;
    }
    if (lata18 == 1) {
      resposta += `${lata18} lata de 18 litros de tinta`;
    }
    maisLata = true;
  }
  if (lata3_6 > 0) {
    if (maisLata) resposta += " + ";
    if (lata3_6 > 1) {
      resposta += `${lata3_6} latas de 3,6L de tinta,`;
    }
    if (lata3_6 == 1) {
      resposta += `${lata3_6} lata de 3,6L de tinta`;
    }
    maisLata = true;
  }

  if (lata2_5 > 0) {
    if (maisLata) resposta += " + ";
    if (lata2_5 > 1) {
      resposta += `${lata2_5} latas de 2,5L de tinta,`;
    }
    if (lata2_5 == 1) {
      resposta += `${lata2_5} lata de 2,5L de tinta`;
    }
    maisLata = true;
  }
  if (lata05 > 0) {
    if (maisLata) resposta += " + ";
    if (lata05 > 1) {
      resposta += `${lata05} latas de 0,5L de tinta`;
    }
    if (lata05 == 1) {
      resposta += `${lata05} lata de 0,5L de tinta`;
    }
    maisLata = true;
  }

  
  // Nesse if está sendo feita a verificação se todas as condições foram atendidas e só assim mostra o resultado final pedido no codigo
  if (cond1 && cond2 && cond3 && cond4) {
    parede.innerHTML = `Sua área total é de ${totalArea} m²`;
    tinta.innerHTML = resposta;
  }

  console.log(cond1, cond2, cond3, cond4);
  console.log(lata05, lata2_5, lata3_6, lata18);

});
